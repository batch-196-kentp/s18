console.log("Hello World")

function addNum(num1,num2){
	let sum = num1 + num2;

	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}

addNum(5,15);

function subtractNum(num1,num2){
	let difference = num1 - num2;

	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);
}

subtractNum(20,5)

function multiplyNum(num1,num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	let product = num1 * num2;
	return product;

}

let product = multiplyNum(50,10);
console.log(product);

function divideNum(num1,num2) {
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	let quotient = num1 / num2;
	return quotient;
}

let quotient = divideNum(50,10);
console.log(quotient);

function calculateAreaOfCircle(r){
	console.log("The result of getting the are of a circle with " + r + " radius:")
	let pi = 3.14;
	let circleArea = pi * (r ** 2);
	return circleArea;
}

let circleArea = calculateAreaOfCircle(15);
console.log(circleArea);

function findAverage(num1,num2,num3,num4){
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4 + ":");
	let average = (num1 + num2 + num3 + num4) / 4;
	return average;
}

let averageVar = findAverage(20,40,60,80);
console.log(averageVar);

function calculateGrade(num1,num2){
	console.log("Is " + num1 + "/" + num2 + " a passing score?")
	let gradeAverage = (num2 / num1) * 100;
	let passingGrade = gradeAverage >= 75;
	return passingGrade;
}

let isPassed = calculateGrade(38,50);
console.log(isPassed);